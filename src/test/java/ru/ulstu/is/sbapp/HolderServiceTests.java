package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.dto.HolderDto;
import ru.ulstu.is.sbapp.exception.ResourceNotFoundException;
import ru.ulstu.is.sbapp.service.HolderService;

import javax.transaction.Transactional;
import java.util.Arrays;

@SpringBootTest
public class HolderServiceTests {

    private static final Logger log = LoggerFactory.getLogger(HolderServiceTests.class);

    @Autowired
    private HolderService holderService;

    @AfterEach
    void clearEntities() {
        holderService.deleteAllHolders();
    }

    @Test
    void testHolderCreate() {
        String holderName = "Pavel Kurmyza";
        String[] holderCars = new String[]{"Ferrari", "Lamborghini"};

        HolderDto holderDto = new HolderDto();
        holderDto.setName(holderName);
        holderDto.setCars(Arrays.asList(holderCars));

        final HolderDto createdHolderDto = holderService.addHolder(holderDto);
        log.info(createdHolderDto.toString());

        Assertions.assertNotNull(createdHolderDto.getId());
        Assertions.assertEquals(createdHolderDto.getName(), holderName);
    }

    @Test
    @Transactional
    void testHolderRead() {
        String holderName = "Pavel Kurmyza";
        String[] holderCars = new String[]{"Ferrari", "Lamborghini"};

        HolderDto holderDto = new HolderDto();
        holderDto.setName(holderName);
        holderDto.setCars(Arrays.asList(holderCars));

        final HolderDto createdHolderDto = holderService.addHolder(holderDto);
        log.info(createdHolderDto.toString());

        final HolderDto foundHolderDto = holderService.getHolder(createdHolderDto.getId());
        log.info(foundHolderDto.toString());

        Assertions.assertEquals(createdHolderDto, foundHolderDto);
    }

    @Test
    @Transactional
    void testHolderReadNotFound() {
        Assertions.assertThrows(ResourceNotFoundException.class, () -> holderService.getHolder(-1L));
    }

    @Test
    @Transactional
    void testHolderReadAll() {
        String holderName1 = "Pavel Kurmyza";
        String[] holderCars1 = new String[]{"Ferrari", "Lamborghini"};

        HolderDto holderDto1 = new HolderDto();
        holderDto1.setName(holderName1);
        holderDto1.setCars(Arrays.asList(holderCars1));

        final HolderDto createdHolderDto1 = holderService.addHolder(holderDto1);
        log.info(createdHolderDto1.toString());

        String holderName2 = "Kirill Gorokhov";
        String[] holderCars2 = new String[]{"Lada", "Niva"};

        HolderDto holderDto2 = new HolderDto();
        holderDto2.setName(holderName2);
        holderDto2.setCars(Arrays.asList(holderCars2));

        final HolderDto createdHolderDto2 = holderService.addHolder(holderDto2);
        log.info(createdHolderDto2.toString());

        Assertions.assertEquals(2, holderService.getAllHolders().size());
    }

    @Test
    @Transactional
    void testHolderUpdate() {
        String holderName = "Pavel Kurmyza";
        String[] holderCars = new String[]{"Ferrari", "Lamborghini"};

        HolderDto holderDto = new HolderDto();
        holderDto.setName(holderName);
        holderDto.setCars(Arrays.asList(holderCars));

        final HolderDto createdHolderDto = holderService.addHolder(holderDto);
        log.info(createdHolderDto.toString());

        String newHolderName = "Kurmyza Pavel";

        HolderDto newHolderDto = new HolderDto();
        newHolderDto.setName(newHolderName);
        newHolderDto.setCars(holderDto.getCars());

        final HolderDto updatedHolderDto = holderService.updateHolder(createdHolderDto.getId(), newHolderDto);
        log.info(updatedHolderDto.toString());

        Assertions.assertEquals(createdHolderDto.getId(), updatedHolderDto.getId());
        Assertions.assertEquals(updatedHolderDto.getName(), newHolderName);
    }

    @Test
    @Transactional
    void testHolderDelete() {
        String holderName = "Pavel Kurmyza";
        String[] holderCars = new String[]{"Ferrari", "Lamborghini"};

        HolderDto holderDto = new HolderDto();
        holderDto.setName(holderName);
        holderDto.setCars(Arrays.asList(holderCars));

        final HolderDto createdHolderDto = holderService.addHolder(holderDto);
        log.info(createdHolderDto.toString());

        Assertions.assertEquals(1, holderService.getAllHolders().size());

        holderService.deleteHolder(createdHolderDto.getId());

        Assertions.assertEquals(0, holderService.getAllHolders().size());
    }
}
