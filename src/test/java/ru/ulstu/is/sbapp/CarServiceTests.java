package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.dto.CarDto;
import ru.ulstu.is.sbapp.exception.ResourceNotFoundException;
import ru.ulstu.is.sbapp.service.CarService;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;

@SpringBootTest
public class CarServiceTests {

    private static final Logger log = LoggerFactory.getLogger(CarServiceTests.class);

    @Autowired
    private CarService carService;

    @AfterEach
    void clearEntities() {
        carService.deleteAllCars();
    }

    @Test
    void testCarCreate() {
        String carModel = "BMW";
        String carHolder = "Kurmyza Pavel";
        String[] carManufacturers = new String[]{"Germany", "France"};

        CarDto carDto = new CarDto();
        carDto.setModel(carModel);
        carDto.setHolderName(carHolder);
        carDto.setManufacturers(new HashSet<>(Arrays.asList(carManufacturers)));

        final CarDto createdCarDto = carService.addCar(carDto);
        log.info(createdCarDto.toString());

        Assertions.assertNotNull(createdCarDto.getId());
        Assertions.assertEquals(createdCarDto.getModel(), carModel);
    }

    @Test
    @Transactional
    void testCarRead() {
        String carModel = "BMW";
        String carHolder = "Kurmyza Pavel";
        String[] carManufacturers = new String[]{"Germany", "France"};

        CarDto carDto = new CarDto();
        carDto.setModel(carModel);
        carDto.setHolderName(carHolder);
        carDto.setManufacturers(new HashSet<>(Arrays.asList(carManufacturers)));

        final CarDto createdCarDto = carService.addCar(carDto);
        log.info(createdCarDto.toString());

        final CarDto foundCarDto = carService.getCar(createdCarDto.getId());
        log.info(foundCarDto.toString());

        Assertions.assertEquals(createdCarDto, foundCarDto);
    }

    @Test
    @Transactional
    void testCarReadNotFound() {
        Assertions.assertThrows(ResourceNotFoundException.class, () -> carService.getCar(-1L));
    }

    @Test
    @Transactional
    void testCarReadAll() {
        String car1Model = "BMW";
        String car1Holder = "Kurmyza Pavel";
        String[] car1Manufacturers = new String[]{"Germany", "France"};

        CarDto car1Dto = new CarDto();
        car1Dto.setModel(car1Model);
        car1Dto.setHolderName(car1Holder);
        car1Dto.setManufacturers(new HashSet<>(Arrays.asList(car1Manufacturers)));

        final CarDto createdCarDto1 = carService.addCar(car1Dto);
        log.info(createdCarDto1.toString());

        String car2Model = "Ferrari";
        String car2Holder = "Gorokhov Kirill";
        String[] car2Manufacturers = new String[]{"Russia", "France"};

        CarDto car2Dto = new CarDto();
        car2Dto.setModel(car2Model);
        car2Dto.setHolderName(car2Holder);
        car2Dto.setManufacturers(new HashSet<>(Arrays.asList(car2Manufacturers)));

        final CarDto createdCarDto2 = carService.addCar(car2Dto);
        log.info(createdCarDto2.toString());

        Assertions.assertEquals(2, carService.getAllCars().size());
    }

    @Test
    @Transactional
    void testCarUpdate() {
        String carModel = "BMW";
        String carHolder = "Kurmyza Pavel";
        String[] carManufacturers = new String[]{"Germany", "France"};

        CarDto carDto = new CarDto();
        carDto.setModel(carModel);
        carDto.setHolderName(carHolder);
        carDto.setManufacturers(new HashSet<>(Arrays.asList(carManufacturers)));

        final CarDto createdCarDto = carService.addCar(carDto);
        log.info(createdCarDto.toString());

        String newCarModel = "WMB";

        CarDto newCarDto = new CarDto();
        newCarDto.setModel(newCarModel);
        newCarDto.setHolderName(carHolder);
        newCarDto.setManufacturers(carDto.getManufacturers());

        final CarDto updatedCarDto = carService.updateCar(createdCarDto.getId(), newCarDto);
        log.info(updatedCarDto.toString());

        Assertions.assertEquals(createdCarDto.getId(), updatedCarDto.getId());
        Assertions.assertEquals(updatedCarDto.getModel(), newCarModel);
        Assertions.assertEquals(updatedCarDto.getHolderName(), carHolder);
    }

    @Test
    @Transactional
    void testCarDelete() {
        String carModel = "BMW";
        String carHolder = "Kurmyza Pavel";
        String[] carManufacturers = new String[]{"Germany", "France"};

        CarDto carDto = new CarDto();
        carDto.setModel(carModel);
        carDto.setHolderName(carHolder);
        carDto.setManufacturers(new HashSet<>(Arrays.asList(carManufacturers)));

        final CarDto createdCarDto = carService.addCar(carDto);
        log.info(createdCarDto.toString());

        Assertions.assertEquals(1, carService.getAllCars().size());

        carService.deleteCar(createdCarDto.getId());

        Assertions.assertEquals(0, carService.getAllCars().size());
    }
}
