package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.dto.ManufacturerDto;
import ru.ulstu.is.sbapp.exception.ResourceNotFoundException;
import ru.ulstu.is.sbapp.service.ManufacturerService;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;

@SpringBootTest
public class ManufacturerServiceTests {

    private static final Logger log = LoggerFactory.getLogger(ManufacturerServiceTests.class);

    @Autowired
    private ManufacturerService manufacturerService;

    @AfterEach
    void clearEntities() {
        manufacturerService.deleteAllManufacturers();
    }

    @Test
    void testManufacturerCreate() {
        String manufacturerCountry = "Russia";
        String[] manufacturerCars = new String[]{"Lada", "Niva"};

        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setCountry(manufacturerCountry);
        manufacturerDto.setCars(new HashSet<>(Arrays.asList(manufacturerCars)));

        final ManufacturerDto createdManufacturerDto = manufacturerService.addManufacturer(manufacturerDto);
        log.info(createdManufacturerDto.toString());

        Assertions.assertNotNull(createdManufacturerDto.getId());
        Assertions.assertEquals(createdManufacturerDto.getCountry(), manufacturerCountry);
    }

    @Test
    @Transactional
    void testManufacturerRead() {
        String manufacturerCountry = "Russia";
        String[] manufacturerCars = new String[]{"Lada", "Niva"};

        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setCountry(manufacturerCountry);
        manufacturerDto.setCars(new HashSet<>(Arrays.asList(manufacturerCars)));

        final ManufacturerDto createdManufacturerDto = manufacturerService.addManufacturer(manufacturerDto);
        log.info(createdManufacturerDto.toString());

        final ManufacturerDto foundManufacturerDto = manufacturerService.getManufacturer(createdManufacturerDto.getId());
        log.info(foundManufacturerDto.toString());

        Assertions.assertEquals(createdManufacturerDto, foundManufacturerDto);
    }

    @Test
    @Transactional
    void testManufacturerReadNotFound() {
        Assertions.assertThrows(ResourceNotFoundException.class, () -> manufacturerService.getManufacturer(-1L));
    }

    @Test
    @Transactional
    void testManufacturerReadAllEmpty() {
        String manufacturerCountry1 = "Russia";
        String[] manufacturerCars1 = new String[]{"Lada", "Niva"};

        ManufacturerDto manufacturerDto1 = new ManufacturerDto();
        manufacturerDto1.setCountry(manufacturerCountry1);
        manufacturerDto1.setCars(new HashSet<>(Arrays.asList(manufacturerCars1)));

        final ManufacturerDto createdManufacturerDto1 = manufacturerService.addManufacturer(manufacturerDto1);
        log.info(createdManufacturerDto1.toString());

        String manufacturerCountry2 = "Germany";
        String[] manufacturerCars2 = new String[]{"BMW", "Volkswagen"};

        ManufacturerDto manufacturerDto2 = new ManufacturerDto();
        manufacturerDto2.setCountry(manufacturerCountry2);
        manufacturerDto2.setCars(new HashSet<>(Arrays.asList(manufacturerCars2)));

        final ManufacturerDto createdManufacturerDto2 = manufacturerService.addManufacturer(manufacturerDto2);
        log.info(createdManufacturerDto2.toString());

        manufacturerService.deleteManufacturer(createdManufacturerDto1.getId());
        manufacturerService.deleteManufacturer(createdManufacturerDto2.getId());

        Assertions.assertEquals(0, manufacturerService.getAllManufacturers().size());
    }

    @Test
    @Transactional
    void testManufacturerUpdate() {
        String manufacturerCountry = "Russia";
        String[] manufacturerCars = new String[]{"Lada", "Niva"};

        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setCountry(manufacturerCountry);
        manufacturerDto.setCars(new HashSet<>(Arrays.asList(manufacturerCars)));

        final ManufacturerDto createdManufacturerDto = manufacturerService.addManufacturer(manufacturerDto);
        log.info(createdManufacturerDto.toString());

        String newManufacturerCountry = "Russian Federation";

        ManufacturerDto newManufacturerDto = new ManufacturerDto();
        newManufacturerDto.setCountry(newManufacturerCountry);

        final ManufacturerDto updatedManufacturerDto = manufacturerService.updateManufacturer(createdManufacturerDto.getId(), newManufacturerDto);
        log.info(updatedManufacturerDto.toString());

        Assertions.assertEquals(createdManufacturerDto.getId(), updatedManufacturerDto.getId());
        Assertions.assertEquals(updatedManufacturerDto.getCountry(), newManufacturerCountry);
    }

    @Test
    @Transactional
    void testManufacturerDelete() {
        String manufacturerCountry = "Russia";
        String[] manufacturerCars = new String[]{"Lada", "Niva"};

        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setCountry(manufacturerCountry);
        manufacturerDto.setCars(new HashSet<>(Arrays.asList(manufacturerCars)));

        final ManufacturerDto createdManufacturerDto = manufacturerService.addManufacturer(manufacturerDto);
        log.info(createdManufacturerDto.toString());

        Assertions.assertEquals(1, manufacturerService.getAllManufacturers().size());

        manufacturerService.deleteManufacturer(createdManufacturerDto.getId());

        Assertions.assertEquals(0, manufacturerService.getAllManufacturers().size());
    }
}
