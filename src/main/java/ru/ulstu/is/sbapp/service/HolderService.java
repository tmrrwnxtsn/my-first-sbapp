package ru.ulstu.is.sbapp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ulstu.is.sbapp.dto.HolderDto;
import ru.ulstu.is.sbapp.exception.ResourceNotFoundException;
import ru.ulstu.is.sbapp.model.Car;
import ru.ulstu.is.sbapp.model.Holder;
import ru.ulstu.is.sbapp.repository.CarRepository;
import ru.ulstu.is.sbapp.repository.HolderRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class HolderService {

    @Resource
    private HolderRepository holderRepository;

    @Resource
    private CarRepository carRepository;

    @Transactional
    public HolderDto addHolder(HolderDto holderDto) {
        Holder holder = new Holder();
        mapDtoToEntity(holderDto, holder);
        Holder savedHolder = holderRepository.save(holder);
        return mapEntityToDto(savedHolder);
    }

    @Transactional(readOnly = true)
    public HolderDto getHolder(Long id) {
        return mapEntityToDto(holderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Holder with id=" + id + " not found")));
    }

    @Transactional(readOnly = true)
    public List<HolderDto> getAllHolders() {
        List<HolderDto> holderDtos = new ArrayList<>();
        List<Holder> holders = holderRepository.findAll();
        holders.forEach(holder -> {
            HolderDto holderDto = mapEntityToDto(holder);
            if (holderDto.getName() != null && !Objects.equals(holderDto.getName(), "")) {
                holderDtos.add(holderDto);
            }
        });
        return holderDtos;
    }

    @Transactional
    public HolderDto updateHolder(Long id, HolderDto holderDto) {
        return holderRepository.findById(id).map(holder -> {
            holder.removeCars();
            mapDtoToEntity(holderDto, holder);
            return mapEntityToDto(holderRepository.save(holder));
        }).orElseThrow(() -> new ResourceNotFoundException("Holder with id=" + id + " not found"));
    }

    @Transactional
    public String deleteHolder(Long id) {
        return holderRepository.findById(id).map(holder -> {
            holder.removeCars();
            holderRepository.deleteById(holder.getId());
            return "Holder with id=" + id + " successfully deleted";
        }).orElseThrow(() -> new ResourceNotFoundException("Holder with id=" + id + " not found"));
    }

    @Transactional
    public void deleteAllHolders() {
        holderRepository.deleteAll();
    }

    private void mapDtoToEntity(HolderDto holderDto, Holder holder) {
        holder.setName(holderDto.getName());
        if (holder.getCars() == null || holderDto.getCars() == null || holderDto.getCars().size() == 0) {
            holder.setCars(new ArrayList<>());
        }
        holderDto.getCars().forEach(model -> {
            Car car = carRepository.findByModel(model);
            if (car == null) {
                car = new Car();
                car.setModel(model);
            }
            holder.addCar(car);
        });
    }

    private HolderDto mapEntityToDto(Holder holder) {
        HolderDto responseDto = new HolderDto();
        responseDto.setId(holder.getId());
        responseDto.setName(holder.getName());
        responseDto.setCars(holder.getCars().stream().map(Car::getModel).collect(Collectors.toList()));
        return responseDto;
    }
}
