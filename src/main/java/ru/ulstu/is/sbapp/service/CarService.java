package ru.ulstu.is.sbapp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ulstu.is.sbapp.dto.CarDto;
import ru.ulstu.is.sbapp.exception.ResourceNotFoundException;
import ru.ulstu.is.sbapp.model.Car;
import ru.ulstu.is.sbapp.model.Holder;
import ru.ulstu.is.sbapp.model.Manufacturer;
import ru.ulstu.is.sbapp.repository.CarRepository;
import ru.ulstu.is.sbapp.repository.HolderRepository;
import ru.ulstu.is.sbapp.repository.ManufacturerRepository;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CarService {

    @Resource
    private CarRepository carRepository;

    @Resource
    private ManufacturerRepository manufacturerRepository;

    @Resource
    private HolderRepository holderRepository;

    @Resource
    private final ValidatorUtil validatorUtil;

    public CarService(CarRepository carRepository, ManufacturerRepository manufacturerRepository, HolderRepository holderRepository, ValidatorUtil validatorUtil) {
        this.carRepository = carRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.holderRepository = holderRepository;
        this.validatorUtil = validatorUtil;
    }

    @Transactional
    public CarDto addCar(CarDto carDto) {
        validatorUtil.validate(carDto);

        Car car = new Car();
        mapDtoToEntity(carDto, car);
        Car savedCar = carRepository.save(car);
        return mapEntityToDto(savedCar);
    }

    @Transactional(readOnly = true)
    public CarDto getCar(Long id) {
        return mapEntityToDto(carRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Car with id=" + id + " not found")));
    }

    @Transactional(readOnly = true)
    public List<CarDto> getAllCars() {
        List<CarDto> carDtos = new ArrayList<>();
        List<Car> cars = carRepository.findAll();
        cars.forEach(car -> {
            CarDto carDto = mapEntityToDto(car);
            if (carDto.getModel() != null && !Objects.equals(carDto.getModel(), "")) {
                carDtos.add(carDto);
            }
        });
        return carDtos;
    }

    @Transactional
    public CarDto updateCar(Long id, CarDto carDto) {
        return carRepository.findById(id).map(car -> {
            car.getManufacturers().clear();
            mapDtoToEntity(carDto, car);
            return mapEntityToDto(carRepository.save(car));
        }).orElseThrow(() -> new ResourceNotFoundException("Car with id=" + id + " not found"));
    }

    @Transactional
    public String deleteCar(Long id) {
        return carRepository.findById(id).map(car -> {
            car.removeManufacturers();
            carRepository.deleteById(car.getId());
            return "Car with id=" + id + " successfully deleted";
        }).orElseThrow(() -> new ResourceNotFoundException("Car with id=" + id + " not found"));
    }

    @Transactional
    public void deleteAllCars() {
        carRepository.deleteAll();
    }

    private void mapDtoToEntity(CarDto carDto, Car car) {
        car.setModel(carDto.getModel());
        if (car.getManufacturers() == null) {
            car.setManufacturers(new HashSet<>());
        }

        Holder holder = holderRepository.findByName(carDto.getHolderName());
        boolean isNewHolder = false;
        if (holder == null) {
            holder = new Holder();
            holder.setCars(new ArrayList<>());
            isNewHolder = true;
        }
        holder.setName(carDto.getHolderName());
        holder.addCar(car);
        if (isNewHolder) {
            holderRepository.save(holder);
        }

        carDto.getManufacturers().forEach(country -> {
            Manufacturer manufacturer = manufacturerRepository.findByCountry(country);
            if (manufacturer == null) {
                manufacturer = new Manufacturer();
                manufacturer.setCars(new HashSet<>());
            }
            manufacturer.setCountry(country);
            car.addManufacturer(manufacturer);
        });
    }

    private CarDto mapEntityToDto(Car car) {
        CarDto responseDto = new CarDto();
        responseDto.setId(car.getId());
        responseDto.setModel(car.getModel());
        if (car.getHolder() != null) {
            responseDto.setHolderName(car.getHolder().getName());
        }
        responseDto.setManufacturers(car.getManufacturers().stream().map(Manufacturer::getCountry).collect(Collectors.toSet()));
        return responseDto;
    }
}
