package ru.ulstu.is.sbapp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ulstu.is.sbapp.dto.ManufacturerDto;
import ru.ulstu.is.sbapp.exception.ResourceNotFoundException;
import ru.ulstu.is.sbapp.model.Car;
import ru.ulstu.is.sbapp.model.Manufacturer;
import ru.ulstu.is.sbapp.repository.CarRepository;
import ru.ulstu.is.sbapp.repository.ManufacturerRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ManufacturerService {

    @Resource
    private CarRepository carRepository;

    @Resource
    private ManufacturerRepository manufacturerRepository;

    @Transactional
    public ManufacturerDto addManufacturer(ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = new Manufacturer();
        mapDtoToEntity(manufacturerDto, manufacturer);
        Manufacturer savedManufacturer = manufacturerRepository.save(manufacturer);
        return mapEntityToDto(savedManufacturer);
    }

    @Transactional(readOnly = true)
    public ManufacturerDto getManufacturer(Long id) {
        return mapEntityToDto(manufacturerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Manufacturer with id=" + id + " not found")));
    }

    @Transactional(readOnly = true)
    public List<ManufacturerDto> getAllManufacturers() {
        List<ManufacturerDto> manufacturerDtos = new ArrayList<>();
        List<Manufacturer> manufacturers = manufacturerRepository.findAll();
        manufacturers.forEach(manufacturer -> {
            ManufacturerDto manufacturerDto = mapEntityToDto(manufacturer);
            if (manufacturerDto.getCountry() != null && !Objects.equals(manufacturerDto.getCountry(), "")) {
                manufacturerDtos.add(manufacturerDto);
            }
        });
        return manufacturerDtos;
    }

    @Transactional
    public ManufacturerDto updateManufacturer(Long id, ManufacturerDto manufacturerDto) {
        return manufacturerRepository.findById(id).map(manufacturer -> {
            manufacturer.removeCars();
            mapDtoToEntity(manufacturerDto, manufacturer);
            return mapEntityToDto(manufacturerRepository.save(manufacturer));
        }).orElseThrow(() -> new ResourceNotFoundException("Manufacturer with id=" + id + " not found"));
    }

    @Transactional
    public String deleteManufacturer(Long id) {
        return manufacturerRepository.findById(id).map(manufacturer -> {
            manufacturer.removeCars();
            manufacturerRepository.deleteById(manufacturer.getId());
            return "Manufacturer with id=" + id + " successfully deleted";
        }).orElseThrow(() -> new ResourceNotFoundException("Manufacturer with id=" + id + " not found"));
    }

    @Transactional
    public void deleteAllManufacturers() {
        manufacturerRepository.deleteAll();
    }

    private void mapDtoToEntity(ManufacturerDto manufacturerDto, Manufacturer manufacturer) {
        manufacturer.setCountry(manufacturerDto.getCountry());
        if (manufacturer.getCars() == null) {
            manufacturer.setCars(new HashSet<>());
        }
        manufacturerDto.getCars().forEach(model -> {
            Car car = carRepository.findByModel(model);
            if (car == null) {
                car = new Car();
                car.setManufacturers(new HashSet<>());
            }
            car.setModel(model);
            manufacturer.addCar(car);
        });
    }

    private ManufacturerDto mapEntityToDto(Manufacturer manufacturer) {
        ManufacturerDto responseDto = new ManufacturerDto();
        responseDto.setId(manufacturer.getId());
        responseDto.setCountry(manufacturer.getCountry());
        responseDto.setCars(manufacturer.getCars().stream().map(Car::getModel).collect(Collectors.toSet()));
        return responseDto;
    }
}
