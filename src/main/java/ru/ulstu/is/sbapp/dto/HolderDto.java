package ru.ulstu.is.sbapp.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HolderDto {

    private Long id;

    private String name;

    private List<String> cars = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCars() {
        return cars;
    }

    public void setCars(List<String> cars) {
        this.cars = cars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HolderDto holderDto = (HolderDto) o;
        return Objects.equals(id, holderDto.id) && Objects.equals(name, holderDto.name) && Objects.equals(cars, holderDto.cars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cars);
    }

    @Override
    public String toString() {
        return "HolderDto{" + "id=" + id + ", name='" + name + '\'' + ", cars=" + cars + '}';
    }
}
