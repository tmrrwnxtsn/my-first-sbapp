package ru.ulstu.is.sbapp.dto;

import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CarDto {

    private Long id;

    private String model;

    private String holderName;

    @NotEmpty(message = "Car must have one ore more manufacturers!")
    private Set<String> manufacturers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Set<String> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(Set<String> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarDto carDto = (CarDto) o;
        return Objects.equals(id, carDto.id) && Objects.equals(model, carDto.model) && Objects.equals(holderName, carDto.holderName) && Objects.equals(manufacturers, carDto.manufacturers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, model, holderName, manufacturers);
    }

    @Override
    public String toString() {
        return "CarDto{" + "id=" + id + ", model='" + model + '\'' + ", holderName='" + holderName + '\'' + ", manufacturers=" + manufacturers + '}';
    }
}
