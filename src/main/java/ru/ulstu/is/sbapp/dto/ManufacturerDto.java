package ru.ulstu.is.sbapp.dto;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ManufacturerDto {

    private Long id;

    private String country;

    private Set<String> cars = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Set<String> getCars() {
        return cars;
    }

    public void setCars(Set<String> cars) {
        this.cars = cars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ManufacturerDto that = (ManufacturerDto) o;
        return Objects.equals(id, that.id) && Objects.equals(country, that.country) && Objects.equals(cars, that.cars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country, cars);
    }

    @Override
    public String toString() {
        return "ManufacturerDto{" + "id=" + id + ", country='" + country + '\'' + ", cars=" + cars + '}';
    }
}
