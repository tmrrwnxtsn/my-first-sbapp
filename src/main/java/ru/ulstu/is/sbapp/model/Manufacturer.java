package ru.ulstu.is.sbapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Manufacturer {

    private Long id;

    private String country;

    private Set<Car> cars;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @ManyToMany(mappedBy = "manufacturers")
    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    public void addCar(Car car) {
        this.cars.add(car);
        car.getManufacturers().add(this);
    }

    public void removeCar(Car car) {
        this.getCars().remove(car);
        car.getManufacturers().remove(this);
    }

    public void removeCars() {
        for (Car car : new HashSet<>(cars)) {
            removeCar(car);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manufacturer that = (Manufacturer) o;
        return Objects.equals(id, that.id) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country);
    }

    @Override
    public String toString() {
        return "Manufacturer{" + "id=" + id + ", country='" + country + '\'' + ", cars=" + cars + '}';
    }
}
