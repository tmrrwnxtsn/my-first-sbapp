package ru.ulstu.is.sbapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Car {

    private Long id;

    private String model;

    private Holder holder;

    private Set<Manufacturer> manufacturers;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "holder_id")
    public Holder getHolder() {
        return holder;
    }

    public void setHolder(Holder holder) {
        this.holder = holder;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(joinColumns = {@JoinColumn(name = "car_id")}, inverseJoinColumns = {@JoinColumn(name = "manufacturer_id")})
    public Set<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(Set<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public void addManufacturer(Manufacturer manufacturer) {
        this.manufacturers.add(manufacturer);
        manufacturer.getCars().add(this);
    }

    public void removeManufacturer(Manufacturer manufacturer) {
        this.getManufacturers().remove(manufacturer);
        manufacturer.getCars().remove(this);
    }

    public void removeManufacturers() {
        for (Manufacturer manufacturer : new HashSet<>(manufacturers)) {
            removeManufacturer(manufacturer);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(id, car.id) && Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, model);
    }

    @Override
    public String toString() {
        return "Car{" + "id=" + id + ", model='" + model + '\'' + ", manufacturers=" + manufacturers + '}';
    }
}
