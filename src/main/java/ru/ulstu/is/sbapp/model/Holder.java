package ru.ulstu.is.sbapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Holder {

    private Long id;

    private String name;

    private List<Car> cars;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "holder")
    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void addCar(Car car) {
        this.cars.add(car);
        car.setHolder(this);
    }

    public void removeCar(Car car) {
        this.getCars().remove(car);
        car.setHolder(null);
    }

    public void removeCars() {
        for (Car car : new ArrayList<>(cars)) {
            removeCar(car);
        }
    }
}
