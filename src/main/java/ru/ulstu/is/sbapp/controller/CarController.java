package ru.ulstu.is.sbapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.WebConfiguration;
import ru.ulstu.is.sbapp.dto.CarDto;
import ru.ulstu.is.sbapp.service.CarService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/cars")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarDto> getCar(@PathVariable(name = "id") Long id) {
        CarDto car = carService.getCar(id);
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<CarDto>> getAllCars() {
        List<CarDto> cars = carService.getAllCars();
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<CarDto> createCar(@RequestBody @Valid CarDto carDto) {
        CarDto std = carService.addCar(carDto);
        return new ResponseEntity<>(std, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CarDto> updateCar(@PathVariable(name = "id") Long id, @RequestBody CarDto car) {
        CarDto std = carService.updateCar(id, car);
        return new ResponseEntity<>(std, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCar(@PathVariable(name = "id") Long id) {
        String message = carService.deleteCar(id);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
