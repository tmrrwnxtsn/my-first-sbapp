package ru.ulstu.is.sbapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.WebConfiguration;
import ru.ulstu.is.sbapp.dto.HolderDto;
import ru.ulstu.is.sbapp.service.HolderService;

import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/holders")
public class HolderController {

    private final HolderService holderService;

    public HolderController(HolderService holderService) {
        this.holderService = holderService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<HolderDto> getHolder(@PathVariable(name = "id") Long id) {
        HolderDto holder = holderService.getHolder(id);
        return new ResponseEntity<>(holder, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<HolderDto>> getAllHolders() {
        List<HolderDto> holders = holderService.getAllHolders();
        return new ResponseEntity<>(holders, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<HolderDto> createHolder(@RequestBody HolderDto holderDto) {
        HolderDto std = holderService.addHolder(holderDto);
        return new ResponseEntity<>(std, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<HolderDto> updateHolder(@PathVariable(name = "id") Long id, @RequestBody HolderDto holder) {
        HolderDto std = holderService.updateHolder(id, holder);
        return new ResponseEntity<>(std, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteHolder(@PathVariable(name = "id") Long id) {
        String message = holderService.deleteHolder(id);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
