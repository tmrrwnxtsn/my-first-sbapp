package ru.ulstu.is.sbapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.WebConfiguration;
import ru.ulstu.is.sbapp.dto.ManufacturerDto;
import ru.ulstu.is.sbapp.service.ManufacturerService;

import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/manufacturers")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;

    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ManufacturerDto> getManufacturer(@PathVariable(name = "id") Long id) {
        ManufacturerDto manufacturer = manufacturerService.getManufacturer(id);
        return new ResponseEntity<>(manufacturer, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<ManufacturerDto>> getAllManufacturers() {
        List<ManufacturerDto> manufacturers = manufacturerService.getAllManufacturers();
        return new ResponseEntity<>(manufacturers, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<ManufacturerDto> createManufacturer(@RequestBody ManufacturerDto manufacturerDto) {
        ManufacturerDto std = manufacturerService.addManufacturer(manufacturerDto);
        return new ResponseEntity<>(std, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ManufacturerDto> updateManufacturer(@PathVariable(name = "id") Long id, @RequestBody ManufacturerDto manufacturerDto) {
        ManufacturerDto std = manufacturerService.updateManufacturer(id, manufacturerDto);
        return new ResponseEntity<>(std, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteManufacturer(@PathVariable(name = "id") Long id) {
        String message = manufacturerService.deleteManufacturer(id);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
