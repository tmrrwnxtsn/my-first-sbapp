package ru.ulstu.is.sbapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.dto.HolderDto;
import ru.ulstu.is.sbapp.service.CarService;
import ru.ulstu.is.sbapp.service.HolderService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/holders")
public class HolderMvcController {

    private final HolderService holderService;

    private final CarService carService;

    public HolderMvcController(HolderService holderService, CarService carService) {
        this.holderService = holderService;
        this.carService = carService;
    }

    @GetMapping
    public String getHolders(Model model) {
        model.addAttribute("holders", holderService.getAllHolders());
        return "holders";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editHolder(@PathVariable(required = false) Long id, Model model) {
        if (id == null || id <= 0) {
            model.addAttribute("holderDto", new HolderDto());
        } else {
            model.addAttribute("holderId", id);
            model.addAttribute("holderDto", holderService.getHolder(id));
        }
        model.addAttribute("allCars", carService.getAllCars());
        return "holder-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveHolder(@PathVariable(required = false) Long id,
                             @ModelAttribute @Valid HolderDto holderDto,
                             BindingResult bindingResult,
                             Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "holder-edit";
        }
        if (id == null || id <= 0) {
            holderService.addHolder(holderDto);
        } else {
            holderService.updateHolder(id, holderDto);
        }
        return "redirect:/holders";
    }

    @PostMapping("/delete/{id}")
    public String deleteHolder(@PathVariable Long id) {
        holderService.deleteHolder(id);
        return "redirect:/holders";
    }
}

