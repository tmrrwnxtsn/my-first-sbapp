package ru.ulstu.is.sbapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.dto.ManufacturerDto;
import ru.ulstu.is.sbapp.service.CarService;
import ru.ulstu.is.sbapp.service.ManufacturerService;

import javax.validation.Valid;

@Controller
@RequestMapping("/manufacturers")
public class ManufacturerMvcController {

    private final ManufacturerService manufacturerService;

    private final CarService carService;

    public ManufacturerMvcController(ManufacturerService manufacturerService, CarService carService) {
        this.manufacturerService = manufacturerService;
        this.carService = carService;
    }

    @GetMapping
    public String getManufacturers(Model model) {
        model.addAttribute("manufacturers", manufacturerService.getAllManufacturers());
        return "manufacturers";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editManufacturer(@PathVariable(required = false) Long id, Model model) {
        if (id == null || id <= 0) {
            model.addAttribute("manufacturerDto", new ManufacturerDto());
        } else {
            model.addAttribute("manufacturerId", id);
            model.addAttribute("manufacturerDto", manufacturerService.getManufacturer(id));
        }
        model.addAttribute("allCars", carService.getAllCars());
        return "manufacturer-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveManufacturer(@PathVariable(required = false) Long id, @ModelAttribute @Valid ManufacturerDto manufacturerDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "manufacturer-edit";
        }
        if (id == null || id <= 0) {
            manufacturerService.addManufacturer(manufacturerDto);
        } else {
            manufacturerService.updateManufacturer(id, manufacturerDto);
        }
        return "redirect:/manufacturers";
    }

    @PostMapping("/delete/{id}")
    public String deleteManufacturer(@PathVariable Long id) {
        manufacturerService.deleteManufacturer(id);
        return "redirect:/manufacturers";
    }
}


