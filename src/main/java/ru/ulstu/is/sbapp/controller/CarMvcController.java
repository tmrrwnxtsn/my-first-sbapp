package ru.ulstu.is.sbapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.dto.CarDto;
import ru.ulstu.is.sbapp.service.CarService;
import ru.ulstu.is.sbapp.service.HolderService;
import ru.ulstu.is.sbapp.service.ManufacturerService;

import javax.validation.Valid;

@Controller
@RequestMapping("/cars")
public class CarMvcController {

    private final CarService carService;

    private final ManufacturerService manufacturerService;

    private final HolderService holderService;

    public CarMvcController(CarService carService, ManufacturerService manufacturerService, HolderService holderService) {
        this.carService = carService;
        this.manufacturerService = manufacturerService;
        this.holderService = holderService;
    }

    @GetMapping
    public String getCars(Model model) {
        model.addAttribute("cars", carService.getAllCars());
        return "cars";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editCar(@PathVariable(required = false) Long id, Model model) {
        if (id == null || id <= 0) {
            model.addAttribute("carDto", new CarDto());
        } else {
            model.addAttribute("carId", id);
            model.addAttribute("carDto", carService.getCar(id));
        }
        model.addAttribute("allManufacturers", manufacturerService.getAllManufacturers());
        model.addAttribute("allHolders", holderService.getAllHolders());
        return "car-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveCar(@PathVariable(required = false) Long id,
                          @ModelAttribute @Valid CarDto carDto,
                          BindingResult bindingResult,
                          Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "car-edit";
        }
        if (id == null || id <= 0) {
            carService.addCar(carDto);
        } else {
            carService.updateCar(id, carDto);
        }
        return "redirect:/cars";
    }

    @PostMapping("/delete/{id}")
    public String deleteCar(@PathVariable Long id) {
        carService.deleteCar(id);
        return "redirect:/cars";
    }
}
