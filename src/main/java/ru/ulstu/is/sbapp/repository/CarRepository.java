package ru.ulstu.is.sbapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.is.sbapp.model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {

    Car findByModel(String model);
}
