package ru.ulstu.is.sbapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.is.sbapp.model.Holder;

public interface HolderRepository extends JpaRepository<Holder, Long> {

    Holder findByName(String name);
}
